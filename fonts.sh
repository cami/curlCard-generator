#!/usr/bin/env bash
######################################
# Font attributes, colors, bg colors #
######################################

# Background colors
bg_black="$(tput setab 0)"
bg_red="$(tput setab 1)"
bg_green="$(tput setab 2)"
bg_yellow="$(tput setab 3)"
bg_blue="$(tput setab 4)"
bg_magenta="$(tput setab 5)"
bg_cyan="$(tput setab 6)"
bg_white="$(tput setab 7)"
bg_default="$(tput setab 9)"

#Foreground colors
black="$(tput setaf 0)"
red="$(tput setaf 1)"
green="$(tput setaf 2)"
yellow="$(tput setaf 3)"
blue="$(tput setaf 4)"
magenta="$(tput setaf 5)"
cyan="$(tput setaf 6)"
white="$(tput setaf 7)"
default="$(tput setaf 9)"

# Text decoration
underline="$(tput smul)"
end_underline="$(tput rmul)"
hidden="$(tput invis)"
reverse="$(tput rev)"
bold="$(tput bold)"
dim="$(tput dim)"
blink="$(tput blink)"

# Reset colors
reset_color="$(tput sgr0)"

# Export Background colors
export bg_black
export bg_red
export bg_green
export bg_yellow
export bg_blue
export bg_magenta
export bg_cyan
export bg_white
export bg_default

# Export Foreground colors
export black
export red
export green
export yellow
export blue
export magenta
export cyan
export white
export default

# export Text decoration
export underline
export end_underline
export hidden
export reverse
export bold
export dim
export blink

# Reset colors
export reset_color
