<div align="center">

# A cURLable business card

This is a fork from [https://github.com/tallguyjenks/BusinessCard](https://github.com/tallguyjenks/BusinessCard).

> “If it’s not cURLable, it’s not on the web.” - Vint Cerf


![Screenshot from output](card_output.png)

</div>

---

## Usage

### Requirements

Before using this script make you have the following programs installed on your local machine:
- jq
- figlet

`jq` is used to read the config from the file `entries.json`. `figlet` is used to create the ascii-art.

### Create own card

First clone this repository as follows:
```
git clone https://codeberg.org/cami/curlCard-generator.git
cd curlCard-generator
```

This shellscript will provide you an easy way to create your own cURLcard. First have a look at the file `fonts.sh`. There are the defined colors available which you can use later.

After that open the file `entries.json` and then you can edit the given options or you can create other entries. In the following there's a description about the json structure and the needed fields.

```json
{
  /*
    Color for the border
    @type Int
    required: yes
  */
  "bordercolor": "magenta",
  /*
    Text which should be display as asciiart
    @type String
    required: yes
  */
  "asciiart": "example.org",
  /*
    Color for the asciiart
    @type String
    required: yes
  */
  "asciicolor": "yellow",
  /*
    different entries for your cURLcard
    @type Array of objects
    required: yes
  */
  "entries": [
    {
      "email": {
        "text": {
          /*
            Text which should be written as a "key"
            @type String
            required: yes
          */
          "text_val": "E-Mail",
          /* 
            Color for the key 
            @type String
            required: no
          */
          "text_color": "",
          /*
            text decoration for the key
            @type String
            required: no
          */
          "text_decoration": "underline"
        },
        "value": {
          /*
            same as above but for the value of the key
          */
          "entry": "mail@example.org",
          "value_color": "",
          "value_decoration": ""
        },
        /* 
            number of empty lines after an entry
            @type Int
            required: yes
        */
        "empty_lines_after": 1
      }
    }
  ]
}

```

After editing the entries you can execute the following command to see your generated cURLcard.
```
bash bc.sh
``` 

If you'd like to have it in a file then you can use one of the following methods. 
The first method will just print the content of the cURLcard into a new file.
```
bash bc.sh > YOUR_CARD.txt
```

If you want to save it in a file and also see the output you can use `tee` as in the following command:
```
bash bc.sh | tee YOUR_CARD.txt
```


## Contribute

Contributions are always welcome!

## License

This program is released under the MIT license, which you can find in the file [LICENSE](LICENSE).
