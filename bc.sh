#!/usr/bin/env bash

OPTION=$1
source fonts.sh
ENTRIES_FILE="entries.json"

TOTAL_WIDTH=65
TYPE_WIDTH=16
BORDER_CHAR="│"
BORDER_COLOR=$(jq '.bordercolor' ${ENTRIES_FILE} | sed 's/"//g')
ASCII_COLOR=$(jq '.asciicolor' ${ENTRIES_FILE} | sed 's/"//g')

# prints the different default lines 
# arguments:
# First argument: line which should get printed
print_line(){
    local START_LINE
    START_LINE="╭───────────────────────────────────────────────────────────────╮"
    local END_LINE
    END_LINE="╰───────────────────────────────────────────────────────────────╯"

    local SPLIT_LINE
    SPLIT_LINE="${BORDER_CHAR}───────────────────────────────────────────────────────────────${BORDER_CHAR}"
    local EMPTY_LINE
    EMPTY_LINE="${BORDER_CHAR}                                                               ${BORDER_CHAR}"
    LINE_TYPE=$1

    case $LINE_TYPE in
        start)
            printf "%b%s%b\n" "${!BORDER_COLOR}${START_LINE}${reset_color}"
            ;;
        end)
            printf "%b%s%b\n" "${!BORDER_COLOR}${END_LINE}${reset_color}"
            ;;
        split)
            printf "%b%s%b\n" "${!BORDER_COLOR}${SPLIT_LINE}${reset_color}"
            ;;
        empty)
            printf "%b%s%b\n" "${!BORDER_COLOR}${EMPTY_LINE}${reset_color}"
            ;;
        *)
            printf "%b%s%b\n" "${!BORDER_COLOR}${EMPTY_LINE}${reset_color}"
            ;;
    esac
}

print_entry(){
    local key_name
    key_name=$1

    local key_value
    key_value=$2

    local key_name_color
    key_name_color=$3
    if [[ "${key_name_color}" == "null" ]]; then
        key_name_color=""
    fi
    local key_name_decoration

    key_name_decoration=$4
    if [[ "${key_name_decoration}" == "null" ]]; then
        key_name_decoration=""
    fi

    local key_value_color
    key_value_color=$5
    if [[ "${key_value_color}" == "null" ]]; then
        key_value_color=""
    fi

    local key_value_decoration
    key_value_decoration=$6
    if [[ "${key_value_decoration}" == "null" ]]; then
        key_value_decoration=""
    fi

    local string
    string="${BORDER_CHAR}  ${key_name}"
    if [[ -z "$key_name_color"  && -n "$key_name_decoration" ]]; then
        col_string="${!BORDER_COLOR}${BORDER_CHAR}${reset_color}  ${!key_name_decoration}${key_name}${reset_color}"

    elif [[ -z "$key_name_decoration" && -n "$key_name_color" ]]; then
        col_string="${!BORDER_COLOR}${BORDER_CHAR}${reset_color}  ${!key_name_color}${key_name}${reset_color}"
    elif [[ -z "$key_name_decoration"  && -z $"$key_name_color" ]]; then
        col_string="${!BORDER_COLOR}${BORDER_CHAR}${reset_color}  ${key_name}"
    else
        col_string="${!BORDER_COLOR}${BORDER_CHAR}${reset_color}  ${!key_name_color}${!key_name_decoration}${key_name}${reset_color}"
    fi
    string_len=${#string}
    # calculate the number of needed spaces 
    diff=$((TYPE_WIDTH - string_len))
    string=$(printf "%s%*s%s\n" "${string}" "${diff}" "" "${key_value}";) 
    if [[ -z "$key_value_color" && -n "$key_value_decoration" ]]; then
        col_string=$(printf "%s%*s%b%s%b\n" "${col_string}" "${diff}" "" "${!key_value_decoration}" "${key_value}" "${reset_color}";)
    elif [[ -z "$key_value_decoration" && -n "$key_value_color" ]]; then
        col_string=$(printf "%s%*s%b%s%b\n" "${col_string}" "${diff}" "" "${!key_value_color}" "${key_value}" "${reset_color}";)
    elif [[ -z "$key_value_decoration" &&  -z "$key_value_color" ]]; then
        col_string=$(printf "%s%*s%s\n" "${col_string}" "${diff}" "" "${key_value}";)
    else
        col_string=$(printf "%s%*s%b%b%s%b\n" "${col_string}" "${diff}" "" "${!key_value_color}" "${!key_value_decoration}" "${key_value}" "${reset_color}";)
    fi
    string_len=${#string}
    diff=$((TOTAL_WIDTH - string_len -1))
    # string; number of spaces; colorcode; border_char, reset_color
    printf "%s%*s%b%s%b\n" "${col_string}" "${diff}" "" "${!BORDER_COLOR}" "${BORDER_CHAR}" "${reset_color}" 
}

print_help(){
    echo "help"
}

print_error(){
    echo "${1}"
}

is_jq_installed(){
    if ! which jq > /dev/null 2>&1; then
        print_error "This program requires jq but it's not installed. Quit."
        exit 1
    fi 
}

is_figlet_installed(){
    if ! which figlet > /dev/null 2>&1; then
        print_error "This program requires figlet but it's not installed. Quit."
        exit 1
    fi 
}

print_ascii() {
    is_figlet_installed
    ascii_text=$(jq -r '.asciiart' "${ENTRIES_FILE}")
    figlet "$ascii_text" > ascii.txt
    while IFS= read -r line; do
        string="${BORDER_CHAR}  ${line}"
        col_string="${!BORDER_COLOR}${BORDER_CHAR}${reset_color}  ${!ASCII_COLOR}${line}${reset_color}"
        string_len=${#string}
        diff=$((TOTAL_WIDTH - string_len -1))
        printf "%s%*s%b%s%b\n" "${col_string}" "${diff}" "" "${!BORDER_COLOR}" "${BORDER_CHAR}" "${reset_color}";
        # printf "%s\n" "${line}";
    done < ascii.txt
}


generate_card(){
    is_jq_installed
    print_line start
    print_ascii
    print_line empty
    print_line split
    local num_entries
    local key_name
    local key_value
    num_entries=$(jq '.entries | length' "${ENTRIES_FILE}")
    for ((i=0; i<num_entries; i++)) ; do
        local key_name
        key_name=$(jq -r \
            '.entries['"$i"'] | .. | objects | select(.text_val) | .text_val' \
            "${ENTRIES_FILE}")

        local key_name_color
        key_name_color=$(jq -r \
            '.entries['"$i"'] | .. | objects | select(.text_color) | .text_color' \
            "${ENTRIES_FILE}")
        if [[ -z "$key_name_color" ]]; then
            key_name_color="null"
        fi

        local key_name_decoration
        key_name_decoration=$(jq -r \
            '.entries['"$i"'] | .. | objects | select(.text_decoration) | .text_decoration' \
            "${ENTRIES_FILE}")
        if [[ -z "$key_name_decoration" ]]; then
            key_name_decoration="null"
        fi

        local key_value_color
        key_value_color=$(jq -r \
            '.entries['"$i"'] | .. | objects | select(.value_color) | .value_color' \
            "${ENTRIES_FILE}")
        if [[ -z "$key_value_color" ]]; then
            key_value_color="null"
        fi

        local key_value_decoration
        key_value_decoration=$(jq -r \
            '.entries['"$i"'] | .. | objects | select(.value_decoration) | .value_decoration' \
            "${ENTRIES_FILE}")
        if [[ -z "$key_value_decoration" ]]; then
            key_value_decoration="null"
        fi

        local key_value
        key_value=$(jq -r \
            '.entries['"$i"'] | .. | objects | select(.entry) | .entry' \
            "${ENTRIES_FILE}")
        print_entry \
            "$key_name" \
            "$key_value" \
            "$key_name_color" \
            "$key_name_decoration" \
            "$key_value_color" \
            "$key_value_decoration"

        lines_after=$(jq -r \
            '.entries['"${i}"'] | .. | objects | select(.empty_lines_after) | .empty_lines_after' \
            "${ENTRIES_FILE}")
            for ((j=0; j < lines_after; j++)); do
                print_line empty
            done
    done
    print_line end
}

case $OPTION in 
    -h|--help)
        print_help
        ;;
    -g|--generate)
        generate_card
        ;;
    *)
        generate_card
        ;;
esac
